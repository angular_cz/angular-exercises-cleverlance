import { Injectable } from '@angular/core';
import { Book } from './model/book';
import { CalculatorService } from './calculator.service';

@Injectable()
export class SavedCalculationsService {

  savedCalculations: Book[] = [];

  constructor(private calculator: CalculatorService) {
  }

  saveCalculation(book: Book) {
    book.price = this.calculator.calculate(book);
    this.savedCalculations.push(book);
  }

}
