import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TemplateBasedModule } from './template-based/template-based.module';
import { SharedModule } from './shared/shared.module';
import { ReactiveBasedModule } from './reactive-based/reactive-based.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SharedModule,
    TemplateBasedModule,
    ReactiveBasedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
