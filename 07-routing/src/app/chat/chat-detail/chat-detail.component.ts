import { Component, OnInit } from '@angular/core';
import { ChatRoom, Message } from '../model/chat';

import { interval, merge, Observable, Subject } from 'rxjs';

import { ActivatedRoute } from '@angular/router';
import { ChatService } from '../chat.service';
import { distinctUntilChanged, skipWhile, startWith, flatMap, tap, map } from 'rxjs/operators';

@Component({
  selector: 'app-chat-detail',
  templateUrl: './chat-detail.component.html'
})
export class ChatDetailComponent implements OnInit {

  chatRoomId: number;
  chatRoom: ChatRoom;
  myMessages$ = new Subject<Message>();
  myMessagesStream$: Observable<Response>;

  routeParams$: Observable<any>;

  constructor(private chatService: ChatService,
              private route: ActivatedRoute) {

    // TODO 2.6 - načtěte id ze snapshotu parametrů
    this.chatRoomId = 1;

    // TODO 3.1 - reagujte na změnu parametrů
    // TODO 3.2 - vytvořte stream změny parametrů

    this.myMessagesStream$ = this.myMessages$
      .pipe(
        flatMap(message => this.chatService.sendMessage(this.chatRoomId, message))
      );
  }

  ngOnInit(): void {

    // TODO 3.3 - připojte stream změny parametrů

    const needsUpdate = merge(
      interval(2000),
      this.myMessagesStream$
    );

    needsUpdate.pipe(
      startWith(null),
      skipWhile(() => this.chatRoomId == null),
      flatMap(() => this.chatService.getRoomById(this.chatRoomId)),
      tap(() => console.log('chatReloaded'))
    ).subscribe((chatRoom) => this.chatRoom = chatRoom);
  }

  sendMyMessage(message: Message) {
    this.myMessages$.next(message);
  }
}
